<?php

/**
 * @file
 * Hooks provided by the BirdSeed module.
 */

/**
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Access\AccessResult;

/**
 * Control access to a BirdSeed script.
 *
 * Modules may implement this hook if they want to disable BirdSeed for some
 * reasons.
 *
 * @return \Drupal\Core\Access\AccessResultInterface|bool|null
 *   - BIRDSEED_ACCESS_ALLOW: If BirdSeed script is allowed.
 *   - BIRDSEED_ACCESS_DENY: If BirdSeed script is disabled.
 *   - BIRDSEED_ACCESS_IGNORE: If script check is
 *
 * @ingroup node_access
 */
function hook_birdseed_access() {
  // Disable for frontpage.
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    return AccessResult::forbidden();
  }
  return AccessResult::neutral();
}

/**
 * Alter results of BirdSeed access check results.
 */
function hook_birdseed_access_alter(&$results) {
  // Force disable for frontpage.
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    $result = AccessResult::forbidden();
  }
  else {
    $result = AccessResult::neutral();
  }
  $results['my_module_check'] = $result;
}

/**
 * @} End of "addtogroup hooks".
 */
