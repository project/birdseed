CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 

INTRODUCTION
------------
Adds the BirdSeed to your website.

What is BirdSeed?
BirdSeed is a website visitor chat and engagement tool which 
supports live chat and video, contact forms and meeting schedules, 
as well as call request forms, email capture, client testimonials, 
a knowledge library, and more.


* For a full description of the module, visit the project page:
https://www.drupal.org/project/birdseed
* To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/birdseed
   

REQUIREMENTS
------------
BirdSeed user account


INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
------------
Having installed the module, go to admin/config/services/birdseed page and fill
the BirdSeed data token field. What is my data token? 
This number is on your script too, and it's on the URL 
when you visit the site dashboard.

The default is set to "Add to every page except the listed pages".
By default the following pages are listed for exclusion:

admin
admin/*
batch
node/add*
node/*/*
user/*/*


MAINTAINERS
------------
Ivan Trokhanenko, https://www.drupal.org/u/i-trokhanenko
