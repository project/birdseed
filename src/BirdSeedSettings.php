<?php

namespace Drupal\birdseed;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class of BirdSeedSettings.
 *
 * @package Drupal\birdseed
 */
class BirdSeedSettings implements BirdSeedSettingsInterface {

  const BIRDSEED_PAGES = "/admin\n/admin/*\n/batch\n/node/add*\n/node/*/*\n/user/*/*";

  /**
   * BirdSeed config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * BirdSeed settings array.
   *
   * @var array
   */
  protected $settings;

  /**
   * BirdSeedSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory
  ) {
    $this->config = $configFactory->get('birdseed.settings');
    $this->getSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings() {
    if (!$this->settings) {
      $settings = (array) $this->config->getOriginal();
      $settings += [
        'data_token' => NULL,
        'enabled' => 1,
        'visibility_pages' => 0,
        'pages' => static::BIRDSEED_PAGES,
        'visibility_roles' => 0,
        'roles' => [],
      ];

      $this->settings = $settings;
    }
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($key, $default = NULL) {
    $this->getSettings();
    return array_key_exists($key, $this->settings) ? $this->settings[$key] : $default;
  }

}
