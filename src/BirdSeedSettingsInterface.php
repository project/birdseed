<?php

namespace Drupal\birdseed;

/**
 * Interface of BirdSeedSettingsInterface.
 *
 * @package Drupal\birdseed
 */
interface BirdSeedSettingsInterface {

  /**
   * Get all settings.
   *
   * @return array
   *   Get settings.
   */
  public function getSettings();

  /**
   * Get setting.
   *
   * @param string $key
   *   Settings key.
   * @param mixed $default
   *   Default value.
   *
   * @return mixed
   *   Setting value.
   */
  public function getSetting($key, $default = NULL);

}
