<?php

namespace Drupal\birdseed;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class of SnippetAccess.
 *
 * @package Drupal\birdseed
 */
class SnippetAccess implements SnippetAccessInterface {

  const ACCESS_ALLOW = TRUE;
  const ACCESS_DENY = FALSE;
  const ACCESS_IGNORE = NULL;

  /**
   * BirdSeed settings.
   *
   * @var \Drupal\birdseed\BirdSeedSettingsInterface
   */
  protected $settings;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Page match.
   *
   * @var bool
   */
  protected $pageMatch;

  /**
   * SnippetAccess constructor.
   *
   * @param \Drupal\birdseed\BirdSeedSettingsInterface $birdseed_settings
   *   BirdSeed settings.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Path\CurrentPathStack $current_path
   *   Current path.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   Alias manager.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Path matcher.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   Current user.
   */
  public function __construct(
    BirdSeedSettingsInterface $birdseed_settings,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    CurrentPathStack $current_path,
    AliasManagerInterface $alias_manager,
    PathMatcherInterface $path_matcher,
    AccountInterface $current_user
  ) {
    $this->settings = $birdseed_settings;
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->currentPath = $current_path;
    $this->aliasManager = $alias_manager;
    $this->pathMatcher = $path_matcher;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function check() {
    if (!$this->settings->getSetting('data_token') || !$this->settings->getSetting('enabled')) {
      return FALSE;
    }

    $result = AccessResult::neutral()
      ->andIf($this->pathCheckResult())
      ->andIf($this->roleCheck());

    $access = [];
    foreach ($this->moduleHandler->getImplementations('birdseed_access') as $module) {
      $module_result = $this->moduleHandler->invoke($module, 'birdseed_access');
      if (is_bool($module_result)) {
        $access[$module] = $module_result;
      }
      elseif ($module_result instanceof AccessResult) {
        $access[$module] = !$module_result->isForbidden();
      }
    }

    $this->moduleHandler->alter('birdseed_access', $access);

    foreach ($access as $module_result) {
      if (is_bool($module_result)) {
        $result = $result->andIf(AccessResult::forbiddenIf(!$module_result));
      }
      elseif ($module_result instanceof AccessResult) {
        $result = $result->andIf($module_result);
      }
    }

    return !$result->isForbidden();
  }

  /**
   * Check path.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Path result.
   */
  protected function pathCheckResult() {
    if (!isset($this->pageMatch)) {
      $visibility = $this->settings->getSetting('visibility_pages');
      $setting_pages = $this->settings->getSetting('pages');

      if (!$setting_pages) {
        $this->pageMatch = TRUE;
        return AccessResult::allowed();
      }

      $pages = mb_strtolower($setting_pages);
      if ($visibility < 2) {
        $path = $this->currentPath->getPath();
        $path_alias = mb_strtolower($this->aliasManager->getAliasByPath($path));
        $path_match = $this->pathMatcher->matchPath($path_alias, $pages);
        $alias_match = (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));
        $this->pageMatch = $path_match || $alias_match;

        // When $visibility has a value of 0, the tracking code is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $this->pageMatch = !($visibility xor $this->pageMatch);
      }
      else {
        $this->pageMatch = FALSE;
      }
    }

    return AccessResult::forbiddenIf(!$this->pageMatch);
  }

  /**
   * Check BirdSeed script should be added for user.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   User result.
   */
  protected function roleCheck() {
    $visibility = $this->settings->getSetting('visibility_roles');
    $enabled = $visibility;
    $roles = $this->settings->getSetting('roles');

    // The birdseed_roles stores the selected roles as an array where
    // the keys are the role IDs. When the role is not selected the
    // value is 0. If a role is selected the value is the role ID.
    $checked_roles = array_filter($roles);
    if (empty($checked_roles)) {
      // No role is selected for BirdSeed, therefore all roles be tracked.
      return AccessResult::allowed();
    }

    if (count(array_intersect($this->currentUser->getRoles(), $checked_roles))) {
      $enabled = !$visibility;
    }

    return AccessResult::forbiddenIf(!$enabled);
  }

}
