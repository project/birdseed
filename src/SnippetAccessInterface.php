<?php

namespace Drupal\birdseed;

/**
 * Interface of SnippetAccessInterface.
 *
 * @package Drupal\birdseed
 */
interface SnippetAccessInterface {

  /**
   * Determines whether we add the script to page.
   *
   * @return bool
   *   Return TRUE if user can view BirdSeed.
   */
  public function check();

}
