<?php

namespace Drupal\birdseed\Form;

use Drupal\birdseed\BirdSeedSettingsInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure BirdSeed settings for the site.
 */
class BirdSeedAdminSettingsForm extends ConfigFormBase {

  /**
   * BirdSeed settings.
   *
   * @var \Drupal\birdseed\BirdSeedSettingsInterface
   */
  protected $birdseedSettings;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SomeClass object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \BirdSeedSettingsInterface $birdseed_settings
   *   The BirdSeed settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    BirdSeedSettingsInterface $birdseed_settings,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($config_factory);
    $this->birdseedSettings = $birdseed_settings;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('birdseed.settings'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'birdseed_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['birdseed.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $role_storage = $this->entityTypeManager->getStorage('user_role');
    $user_roles = $role_storage->loadMultiple();

    // Initialize $role_options array.
    $role_options = [];

    // Extract role names from Role objects.
    if (!empty($user_roles)) {
      foreach ($user_roles as $role) {
        $role_options[$role->id()] = Html::escape($role->label());
      }
    }

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['general']['data_token'] = [
      '#default_value' => $this->birdseedSettings->getSetting('data_token'),
      '#description' => $this->t('Your BirdSeed data-token can be found in your script <code>data-token="<b>95bde5f4321ff929a69a53a36ea9d123</b>"</code> where <code><b>95bde5f4321ff929a69a53a36ea9d123</b></code> is your BirdSeed data-token.'),
      '#maxlength' => 40,
      '#required' => TRUE,
      '#size' => 40,
      '#title' => $this->t('BirdSeed data-token'),
      '#type' => 'textfield',
    ];
    $form['general']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->birdseedSettings->getSetting('enabled'),
      '#description' => $this->t('Uncheck if you want to disable BirdSeed widget for some reason.'),
    ];

    // Visibility settings.
    $visibility = $this->birdseedSettings->getSetting('visibility_pages');
    $pages = $this->birdseedSettings->getSetting('pages');
    $form['tracking']['page_display'] = [
      '#type' => 'details',
      '#title' => $this->t('Pages'),
      '#group' => 'tracking_scope',
      '#open' => TRUE,
    ];

    if ($visibility == 2) {
      $form['tracking']['page_display'] = [];
      $form['tracking']['page_display']['birdseed_visibility_pages'] = [
        '#type' => 'value',
        '#value' => 2,
      ];
      $form['tracking']['page_display']['birdseed_pages'] = [
        '#type' => 'value',
        '#value' => $pages,
      ];
    }
    else {
      $options = [
        $this->t('Every page except the listed pages'),
        $this->t('The listed pages only'),
      ];
      $description_args = [
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>',
      ];
      $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", $description_args);
      $title = $this->t('Pages');

      $form['tracking']['page_display']['birdseed_visibility_pages'] = [
        '#type' => 'radios',
        '#title' => $this->t('Add script to specific pages'),
        '#options' => $options,
        '#default_value' => $visibility,
      ];
      $form['tracking']['page_display']['birdseed_pages'] = [
        '#type' => 'textarea',
        '#title' => $title,
        '#title_display' => 'invisible',
        '#default_value' => $pages,
        '#description' => $description,
        '#rows' => 10,
      ];
    }

    // Render the role overview.
    $visibility_roles = $this->birdseedSettings->getSetting('roles');
    $form['tracking']['role_display'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles'),
      '#group' => 'tracking_scope',
      '#open' => TRUE,
    ];

    $form['tracking']['role_display']['birdseed_visibility_roles'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add script for specific roles'),
      '#options' => [
        $this->t('Add to the selected roles only'),
        $this->t('Add to every role except the selected ones'),
      ],
      '#default_value' => $this->birdseedSettings->getSetting('visibility_roles'),
    ];

    $role_storage = $this->entityTypeManager->getStorage('user_role');
    $user_roles = $role_storage->loadMultiple();

    // Extract role names from Role objects.
    if (!empty($user_roles)) {
      foreach ($user_roles as $role) {
        $role_options[$role->id()] = Html::escape($role->label());
      }
    }
    else {
      // If $user_roles is null, provide an empty array as fallback.
      $role_options = [];
    }

    $form['tracking']['role_display']['birdseed_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#default_value' => !empty($visibility_roles) ? $visibility_roles : [],
      '#options' => $role_options,
      '#description' => $this->t('If none of the roles are selected, all users will see BirdSeed popup. If a user has any of the roles checked, that user will see (or excluded, depending on the setting above).'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Trim some text values.
    $form_state->setValue('data_token', trim($form_state->getValue('data_token')));
    $form_state->setValue('birdseed_pages', trim($form_state->getValue('birdseed_pages')));
    $form_state->setValue('birdseed_roles', array_filter($form_state->getValue('birdseed_roles')));

    // Verify that every path is prefixed with a slash.
    if ($form_state->getValue('birdseed_visibility_pages') != 2) {
      $pages = preg_split('/(\r\n?|\n)/', $form_state->getValue('birdseed_pages'));
      foreach ($pages as $page) {
        if (strpos($page, '/') !== 0 && $page !== '<front>') {
          $form_state->setErrorByName(
            'birdseed_pages',
            $this->t('Path "@page" not prefixed with slash.', ['@page' => $page])
          );
          // Drupal forms show one error only.
          break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('birdseed.settings');
    $config
      ->set('data_token', $form_state->getValue('data_token'))
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('visibility_pages', $form_state->getValue('birdseed_visibility_pages'))
      ->set('pages', $form_state->getValue('birdseed_pages'))
      ->set('visibility_roles', $form_state->getValue('birdseed_visibility_roles'))
      ->set('roles', $form_state->getValue('birdseed_roles'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
